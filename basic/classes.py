#!/usr/bin/python3
"""
Written by QueryVS
License GPL3

"""
from sys import argv as arg_name

print("Name:",arg_name[0])

class obj:
	s=11
	def __init__(self):
		print("__init__ method")
	def func(self):
		print("func function")
	def __del__(self):
		print("__del__ method")

obj().func()

print(obj().s)
