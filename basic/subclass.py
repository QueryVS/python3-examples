#!/usr/bin/python3
"""
Written by QueryVS
License GPL3

"""
from sys import argv as arg_name

print("Name:",arg_name[0])

class obj_subbclass:

	def number(self):
		return 5



class obj(obj_subbclass):

	def __init__(self):
		print(self.number())



obj = obj()
print(obj)
