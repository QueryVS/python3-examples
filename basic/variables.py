#!/usr/bin/python3
"""
Written by QueryVS
License GPL3

Text Type: 	str
Numeric Types: 	int, float, complex
Sequence Types: list, tuple, range
Mapping Type: 	dict
Set Types: 	set, frozenset
Boolean Type: 	bool
Binary Types: 	bytes, bytearray, memoryview


"""
from sys import argv as arg_name


print("Name:",arg_name[0])

str="abc"*2+'str'+("\n"+"q")*4

print(str)

a,b="12",14

c=int(a)+b+1.1111

print(type(c))

